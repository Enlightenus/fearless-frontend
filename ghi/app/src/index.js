import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


// const response = await fetch('http://localhost:8001/api/attendees/');
// console.log(response);
// Does not work
// Module parse failed: The top-level-await experiment is not enabled (set experiments.topLevelAwait: true to enabled it)
// Cannot use the await at top level of JavaScript since React is using Babel to translate the JSX files
// Use async function

async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  console.log(response);
  if (response.ok) {
    const data = await response.json();
    console.log(data);
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
      </React.StrictMode>
    );

  } else {
    console.error(response)
  }

}
loadAttendees();

// The connection worked, but due to CORS access blocked.
// Access to fetch at 'http://localhost:8001/api/attendees/' from origin 'http://localhost:3001'
// has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource.
// Added the CORS_ALLOWED_ORIGINS = ['http://localhost:3001'] in microservices attendees_bc/settings to correct the CORS
// The ghi is under the dearless, so it only need access to the microservices
// Still need to correct fetch issue (TypeError: Failed to fetch)
// make the data to json
