import Nav from './Nav';
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import AttendeesList from './AttendeesList';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import ConferenceForm from './ConferenceForm';
import LocationForm from './LocationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          {/* Add route / to app */}
          {/* <Route path="/" element={<App />} /> */}
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="attendees">
            <Route index element={<AttendeesList attendees={props.attendees} />} />
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route path="*" element={<main><p>Sorry. Nothing Here!</p></main>}>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

// Comment out after moving table to AttendeesList
// import logo from './logo.svg';
// import './App.css';
// import Nav from './Nav';
// // import from Nav

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//     //Here, we don't want to see the other data that is not attendees related.
//   } return (
//     <>
//       <Nav />
//       <div className="container">
//         <table className="table table-dark table-striped">
//           <thead>
//             <tr>
//               <th>Name</th>
//               <th>Conference</th>
//             </tr>
//           </thead>
//           <tbody>
//             {/* for (let attendee of props.attendees) {
//           <tr>
//           <td>{ attendee.name }</td>
//           <td>{ attendee.conference }</td>
//           </tr>
//       } */}
//             {props.attendees.map(attendee => {
//               return (
//                 <tr key={attendee.href}>
//                   <td>{attendee.name}</td>
//                   <td>{attendee.conference}</td>
//                 </tr>
//               );
//             })}
//           </tbody>
//         </table>
//       </div>
//     </>
//   );
// }
// export default App;

// Default config when imported
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
