import React from 'react';


// Basic structure
// class PresentationForm extends React.Component {
// render() {
//     return (
//       <p>Presentation form</p>
//     );
//   }
// export default PresentationForm;

class PresentationForm extends React.Component {
    //build constructor based on what input we expect
    constructor(props) {
        super(props)
        this.state = {
            presenterName: "",
            companyName: "",
            presenterEmail: "",
            title: "",
            synopsis: "",
            //don't forget need one for the entry, one for handling all data
            conference: "",
            conferences: [],
        };

        this.handleConferenceChange = this.handleConferenceChange.bind(this);
        this.handlePresenterNameChange = this.handlePresenterNameChange.bind(this);
        this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
        this.handlePresenterEmailChange = this.handlePresenterEmailChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
        // Need to bind handle submit
        this.handleSubmit = this.handleSubmit.bind(this)

    }
    // get from from html, and update input with closing "/".
    // Replace class to className
    // Replace for to htmlFor
    // Replace selected to value
    //   Before:
    //   <select required name="conference" id="conference" className="form-select">
    //   <option selected value="">Choose a conference</option>
    //   </select>
    //   After:
    //     <select onChange={this.handleConferenceChange} value={this.state.conference} name="conference" id="conference" className="form-select">
    //     <option value="">Choose a conference</option>
    //

    handlePresenterNameChange(event) {
        const value = event.target.value;
        this.setState({ presenterName: value })
    }

    handleCompanyNameChange(event) {
        const value = event.target.value;
        this.setState({ companyName: value })
    }

    handlePresenterEmailChange(event) {
        const value = event.target.value;
        this.setState({ presenterEmail: value })
    }

    handleTitleChange(event) {
        const value = event.target.value;
        this.setState({ title: value });
    }

    handleSynopsisChange(event) {
        const value = event.target.value;
        this.setState({ synopsis: value });
    }

    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({ conference: value });
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ conferences: data.conferences });
        }
    }
    //summit function

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.presenter_name = data.presenterName;
        data.company_name = data.companyName;
        data.presenter_email = data.presenterEmail;
        // data.title= data.title;
        // data.synopsis = data.synopsis;
        // data.conference = data.conference;
        console.log(data)
        console.log(this)

        delete data.presenterName;
        delete data.companyName;
        delete data.presenterEmail;
        // delete extra data that serever don't take. especially delete data.conferences

        delete data.conferences;
        console.log('after delete')
        console.log(data)

        const locationUrl = `http://localhost:8000${this.state.conference}presentations/`;
        // since I am using the href, it includes the api/conference already
        // href: "/api/conferences/3/"

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            this.setState({
                presenterName: "",
                companyName: "",
                presenterEmail: "",
                title: "",
                synopsis: "",
                conference: "",
            });
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        {/* function onSubmit */}
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePresenterNameChange} value={this.state.presenterName} placeholder="Presenter name" required type="text" name="presenter_name"
                                    id="presenter_name" className="form-control" />
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePresenterEmailChange} value={this.state.presenterEmail} placeholder="Presenter email" required type="email" name="presenter_email"
                                    id="presenter_email" className="form-control" />
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleCompanyNameChange} value={this.state.companyName} placeholder="Company name" type="text" name="company_name" id="company_name"
                                    className="form-control" />
                                <label htmlFor="company_name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleTitleChange} value={this.state.title} placeholder="Title" required type="text" name="title" id="title"
                                    className="form-control" />
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea onChange={this.handleSynopsisChange} value={this.state.synopsis} id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleConferenceChange} value={this.state.conference} name="conference" id="conference" className="form-select">
                                    <option value="">Choose a conference</option>
                                    {this.state.conferences.map(conference => {
                                        return (
                                            //id is the key for presentation to get intpo the conference, so we use it as a key.
                                            <option key={conference.href} value={conference.href}>
                                                {conference.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default PresentationForm
