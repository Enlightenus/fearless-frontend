//Explore-card-template
// Remove the card part line 48 and replace with new function
function createCard(name, description, pictureUrl, duration, location) {
    return `
      <div class="card-column" px-4>
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">${duration}</div>
      </div>
    `;
}


// Explore-get-conference-data
// console.log('aaaaaaaaaa')

// window.addEventListener('DOMContentLoaded', async () => {
//     // added async because await is only valid inside async functions and modules
//     console.log('js is loaded')

//     const url = 'http://localhost:8000/api/conferences/';
//     const response = await fetch(url);
//     // fetch returned a promise object.
//     // use await to unwrap the promise object
//     //console.log(response);


//     const data = await response.json();
//     // use response.json() to read the data
//     // response.json() returns a promise which resolves with the result of parsing the body text as JSON.
//     // Note: It is not returning a JSON, but taking JSON
//     console.log(data);
// });

// Alert function
// var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
// var alertTrigger = document.getElementById('liveAlertBtn')

// function alert() {
//   var wrapper = document.createElement('div')
//   wrapper.innerHTML = '<div class="alert alert" role="alert">' + 'ERROR DETECTED!!' + </div>'
//   alertPlaceholder.append(wrapper)
// }


// alert()


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        console.log(url)
        console.log(response)

        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                console.log(detailUrl)
                console.log(detailResponse)
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts).toLocaleDateString()
                    const endDate = new Date(details.conference.ends).toLocaleDateString()
                    const duration = startDate + " - " + endDate
                    const location = details.conference.location.name
                    const html = createCard(title, description, pictureUrl, duration, location);
                    console.log(html);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }
        }

    } catch (e) {
        // Figure out what to do if an error is raised
        console.error('error', error);
    }
})



// Before applying the for-loop
// window.addEventListener('DOMContentLoaded', async () => {
//     const url = 'http://localhost:8000/api/conferences/';
//     try {
//         const response = await fetch(url);
//         console.log(url)
//         console.log(response)

//         if (!response.ok) {
//             // Figure out what to do when the response is bad
//         } else {
//             const data = await response.json();
//             conference = data.conferences[0];
//             console.log(conference);
//             // adding the conference inf o from array inside the JSON
//             const nameTag = document.querySelector('.card-title');
//             nameTag.innerHTML = conference.name;
//             // adjusting the card-title class, h5

//             const detailUrl = `http://localhost:8000${conference.href}`;
//             const detailResponse = await fetch(detailUrl);
//             if (detailResponse.ok) {
//                 const details = await detailResponse.json();
//                 console.log('=====')
//                 console.log(details);
//                 console.log('=====')
//                 //Adding the description
//                 const desNameTag = document.querySelector('.card-text');
//                 desNameTag.innerHTML = details.conference.description;
//                 // adjusting the card-text class

//                 const imageTag = document.querySelector('.card-img-top');
//                 imageTag.src = details.conference.location.picture_url;
//                 // set up the image tag. The Learn told us that it is named as imageTag
//                 // We name it and then replace the .src with the detail.conference...
//                 // Back to the location detail. The info was from the location-List-encoder.
//                 // Added the pic_url
//             }
//         }
//     } catch (e) {
//         console.error('error', error);
//         //what to do if an error is raised
//     }
// })
