console.log("new-location.js is linked")
// add an event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {

    // declare a variable that will hold the URL for the API that just created
    const url = 'http://localhost:8000/api/states/';

    // fetch the URL. use await keyword to get the response, not the Promise.
    const response = await fetch(url);
    console.log(url)
    console.log(response)
    // If the response is okay, then get the data using the .json method.
    // Don't forget to await that, too."
    if (response.ok) {
        const data = await response.json();
        console.log(data)

        // Get the select tag element by its id 'state'
        const stateTag = document.getElementById('state')
        // For each state in the states property of the data
        for (let state of data.states) {
            // Create an 'option' element
            const option = document.createElement("option");
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = state.abbreviation;
            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = state.name;
            // Append the option element as a child of the select tag
            stateTag.appendChild(option)
        }

        const formTag = document.getElementById('create-location-form');

        //add async since we are using await
        formTag.addEventListener('submit', async (event) => {
            event.preventDefault();
            // FormData object expects name attributes on the form inputs
            // go edit the html tag
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            console.log(json);
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);

            }

        });
    }
}
);
