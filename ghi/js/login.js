window.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('login-form');
    form.addEventListener('submit', async event => {
        event.preventDefault();


        // Got a 401
        // Remove this part since login need regular format not the JSON
        //     const data = Object.fromEntries(new FormData(form))
        //     const fetchOptions = {
        //         method: 'post',
        //         body: data,
        //         headers: {
        //             'Content-Type': 'application/json',
        //         }
        //     };

        // wrong
        // const cred = Request.credentials

        // const fetchOptions = {
        //     method: 'post',
        //     body: new FormData(form),
        //     include: cred
        // }

        const fetchOptions = {
            method: 'post',
            body: new FormData(form),
            credentials: 'include',
        }

        const url = 'http://localhost:8000/login/';
        const response = await fetch(url, fetchOptions);
        if (response.ok) {
            window.location.href = '/';
        } else {
            console.error(response);
        }
    });
});
